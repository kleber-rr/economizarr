import 'dart:io';

import 'package:economizarr/model/produto_model.dart';
import 'package:economizarr/screens/listacompras_screen.dart';
import 'package:economizarr/screens/pesquisa_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:geocoder/geocoder.dart';
import 'package:url_launcher/url_launcher.dart';

class FuncoesExtras {

  static const API_KEY = "{AIzaSyAgJVnMW2HNjaMu7x0b5hV4R9knkUB2Fj4}";
  static const String SHARED_PREFS_NAME = "preferences";

  static Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  static Widget getTitleAppBar(BuildContext context, bool home){
    return AppBar(
        title: Row(
          mainAxisAlignment: home ? MainAxisAlignment.start : MainAxisAlignment.spaceBetween,
          children: <Widget>[
            RichText(
              text: TextSpan(
                text: 'ECONOMIZA',
                style: TextStyle(color: Colors.white, fontSize: 20, fontStyle: FontStyle.italic),
                children: <TextSpan>[
                  TextSpan(
                      text: 'RR',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      )),
                ],
              ),
            ),
            !home ? Flexible(child: Image.asset("images/icon_app.webp")) : Container(),
          ],
        ),
        backgroundColor: Theme.of(context).primaryColor,
        leading: home ? Image.asset("images/icon_app_grande.webp") : null,
        centerTitle: true,
      );
  }

  static String getTempoAtualizado(String tempo){

    var dtUpdate = DateTime.parse(tempo);
    var agora = DateTime.now();
    var saldo = agora.difference(dtUpdate);

    var saldoString = saldo.inHours;

    return saldoString.toString() + 'H';

  }

  
  static int getEstado(String estado) {
      switch(estado){
        case "NONE" : return 0;
        case "WAITING" : return 1;
        case "ACTIVE" : return 2;
        case "DONE" : return 3;
        default : return 0;
      }
  }

  static void abrirMaps(Produto produto) async {
    String url = produto.logra + ','+ produto.endNum+','+produto.bairro+','+produto.municipio+','+produto.uf;
    final query = '$url';
    final String googleMapsUrl = "https://www.google.com/maps/search/$url";
    String appleMapsUrl = "https://maps.apple.com/?daddr=$url";
    var address = await Geocoder.local.findAddressesFromQuery(query);
    Address first = address.first;

    try{
      if(Platform.isAndroid){
        if (await canLaunch(googleMapsUrl)) {
          await launch(googleMapsUrl);
        }
      } else if (Platform.isIOS){
        appleMapsUrl = 'http://maps.apple.com/?q=${first.coordinates.latitude},${first.coordinates.longitude}';
        if (await canLaunch(appleMapsUrl)) {
          await launch(appleMapsUrl, forceSafariVC: false);
        }
      } else {
        throw "Couldn't launch URL";
      }
    } catch(err){
      print(err);
    }
        
  }

  static void addToList(Produto produto, BuildContext context){
    Navigator.of(context).push(
      MaterialPageRoute(builder: (context) => ListaComprasScreen(produto: produto,))
    );
  }

  static Future<String> scanningCodeBar() async {
    String barcodeScanRes = await FlutterBarcodeScanner.scanBarcode("#ff6666", "Cancel", true, ScanMode.DEFAULT);
    return barcodeScanRes;
  }

  static void pesquisar(String text, BuildContext context, bool isCombustivel, bool isCodeBar) {
    if(text.isNotEmpty){
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => PesquisaScreen(params: text, isCombustivel: isCombustivel,)
      ));
    }
  }

  static String raioBusca(String val){
    String retorno = "Distância: ";
    switch (val) {
      case "0.0": {retorno = retorno + "2KM";}
        break;
      case "10.0":{retorno = retorno + "5KM";}
        break;
      case "20.0":{retorno = retorno + "10KM";}
        break;
      case "30.0":{retorno = retorno + "SEM LIMITE";}
        break;
      default:{retorno = retorno + "SEM LIMITE";}
        break;
    }
    return retorno;
  }

    static String raioTempo(String val){
    String retorno = "Atualizado em: ";
    switch (val) {
      case "0.0": {retorno = retorno + "1 DIA";}
        break;
      case "10.0":{retorno = retorno + "2 DIAS";}
        break;
      case "20.0":{retorno = retorno + "7 DIAS";}
        break;
      default:{retorno = retorno + "7 DIAS";}
        break;
    }
    return retorno;
  }

}