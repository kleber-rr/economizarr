import 'package:flutter/material.dart';

class ButtonHideShow extends StatelessWidget {
  final Function func;

  ButtonHideShow({@required this.func});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: func,
      child: Container(
        margin: EdgeInsets.all(15),
        height: 5.0,
        width: 120.0,
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
        ),
      ),
    );
  }
}