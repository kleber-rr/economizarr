import 'package:economizarr/utils/funcoes_extras.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ScanearCodeBarProduto extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        String barcodeScanRes = await FuncoesExtras.scanningCodeBar();
        if(barcodeScanRes != "-1")
          FuncoesExtras.pesquisar(barcodeScanRes, context, false, true);
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Expanded(
            child: Container(
              height: 80.0,
              margin: EdgeInsets.all(15),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(const Radius.circular(20.0)),
                  border: Border.all(
                      width: 2, color: Theme.of(context).primaryColor)),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(FontAwesomeIcons.barcode,
                        size: 50, color: Theme.of(context).primaryColor),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "ESCANEAR CODIGO DE BARRAS",
                      style: TextStyle(
                          fontSize: 12, color: Theme.of(context).primaryColor),
                      textAlign: TextAlign.center,
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}