import 'package:economizarr/model/produto_model.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ButtonPlusMinusCounter extends StatefulWidget {
  final Function funcUpdate;
  final Produto produto;

  ButtonPlusMinusCounter({@required this.funcUpdate, @required this.produto});

  @override
  _ButtonPlusMinusCounterState createState() => _ButtonPlusMinusCounterState(funcUpdate, produto);
}

class _ButtonPlusMinusCounterState extends State<ButtonPlusMinusCounter> {

  int _counter;
  Function _funcUpdate;
  Produto _produto;

  _ButtonPlusMinusCounterState(Function funcUpdate, Produto produto){
    _funcUpdate = funcUpdate;
    _produto = produto;
    _counter = _produto.qtdeItens == null ? 1 : _produto.qtdeItens;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.zero,
      margin: EdgeInsets.zero,
      height: 90,
      width: 30,
      decoration: BoxDecoration(
        color: Colors.grey[100],
        borderRadius: BorderRadius.circular(20)
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            height: 30,
            child: IconButton(onPressed:
            (){
              setState(() {
                _counter++;
                _produto.qtdeItens = _counter;
                _funcUpdate(_produto);
              });
              }
              , icon: Icon(FontAwesomeIcons.chevronUp, color: Theme.of(context).accentColor, size: 15,),),
          ),
          Container(height: 30,child: Center(child: Text(_counter.toString(), style: TextStyle(color: Theme.of(context).accentColor, fontSize: 18),))),
          Container(
            height: 30,
            child: IconButton(onPressed: 
            (){
              setState(() {
                if(_counter > 1)
                  _counter--;
                  _produto.qtdeItens = _counter;
                  _funcUpdate(_produto);
              });
              }
              , icon: Icon(FontAwesomeIcons.chevronDown, color: Theme.of(context).accentColor,size: 15,),),
          )
        ],
      )
    );
  }
}