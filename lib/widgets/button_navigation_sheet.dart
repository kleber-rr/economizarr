import 'package:flutter/material.dart';

class ButtonNavigationSheet extends StatelessWidget {
  const ButtonNavigationSheet({
    Key key,
    @required bool stateSheet,
    @required this.iconData,
    @required this.func,
    @required this.title,
    @required this.context,
  }) : _stateSheet = stateSheet, super(key: key);

  final bool _stateSheet;
  final IconData iconData;
  final Function func;
  final String title;
  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Container(
        height: _stateSheet ? 150.0 : 80.0,
        width: _stateSheet ? 
          MediaQuery.of(context).size.width * 0.28 : 
          MediaQuery.of(context).size.width * 0.20,
        margin: EdgeInsets.all(_stateSheet ? 10 : 8),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: _stateSheet
                ? BorderRadius.all(const Radius.circular(20.0))
                : null,
            border: _stateSheet
                ? Border.all(width: 2, color: Theme.of(context).primaryColor)
                : null),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            new Material(
              type: MaterialType.transparency,
              child: IconButton(
                icon: Icon(iconData, color: Theme.of(context).primaryColor),
                onPressed: func,
                highlightColor: Theme.of(context).primaryColor,
                iconSize: _stateSheet ? 60 : 40),
            ),
            Visibility(
                visible: _stateSheet,
                child: Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    title,
                    style: TextStyle(
                        fontSize: 12,
                        color: Theme.of(context).primaryColor,
                        fontWeight: FontWeight.w500),
                    textAlign: TextAlign.center,
                  ),
                ))
          ],
        ),
      ),
    );
  }
}