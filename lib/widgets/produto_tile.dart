import 'package:economizarr/model/produto_model.dart';
import 'package:economizarr/screens/produto_screen.dart';
import 'package:economizarr/utils/funcoes_extras.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ProdutoTile extends StatelessWidget {

  final Produto snapshot;
  final bool isCombustivel;
  final Function func;

  ProdutoTile({@required this.snapshot, @required this.isCombustivel, @required this.func});

  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      child: Container(
              margin: EdgeInsets.only(left: 10, top: 5, bottom: 5, right: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(const Radius.circular(15.0)),
                  ),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.all(10.0),
                        height: 90.0,
                        width: 90.0,
                        margin: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            color: !isCombustivel ? Colors.lightGreen : Colors.yellow,
                            borderRadius: BorderRadius.all(const Radius.circular(15.0)),
                            ),
                        child: Center(
                          child: !isCombustivel ? Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Text(double.parse(snapshot.preco).toStringAsFixed(2), 
                                style: TextStyle(
                                  color: Colors.white, 
                                  fontSize: 20, fontWeight: FontWeight.bold, fontFamily: 'RobotoMono'),),
                              Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Icon(FontAwesomeIcons.barcode, size: 10, color: Colors.white,),
                                    Text("PESQUISAR", style: TextStyle(color: Colors.white, fontSize: 10),)
                                  ],
                                  ),
                            ],
                          ) : Text(double.parse(snapshot.preco).toStringAsFixed(2), 
                                style: TextStyle(
                                  color: Colors.red, 
                                  fontSize: 25, fontWeight: FontWeight.bold, fontFamily: 'RobotoMono'),),
                        ),
                      ),
                      Container(
                        height: 20.0,
                        width: 20.0,
                        margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                        decoration: BoxDecoration(
                            color: !isCombustivel ? Colors.lightGreen[300] : Colors.red,
                            borderRadius: BorderRadius.all(const Radius.circular(20.0)),
                            ),
                        child: Center(child: Text("R\$", style: TextStyle(color: Colors.white, fontSize: 10),)),
                      )
                    ],
                  ),
                  Flexible(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 5.0, top: 10, right: 5, bottom: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(snapshot.produto, style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold), textAlign: TextAlign.left,),
                          Text(" ", style: TextStyle(fontSize: 9.0, fontWeight: FontWeight.w300), textAlign: TextAlign.start,),
                          Text(snapshot.empresa, style: TextStyle(fontSize: 13.0, fontWeight: FontWeight.w300), textAlign: TextAlign.start,),
                          Text(snapshot.bairro + ' - ' + snapshot.municipio, style: TextStyle(fontSize: 13.0, fontWeight: FontWeight.w300), textAlign: TextAlign.start,),
                          Text(" ", style: TextStyle(fontSize: 9.0, fontWeight: FontWeight.w300), textAlign: TextAlign.start,),
                          Row(
                            children: <Widget>[
                              Icon(FontAwesomeIcons.sync, size: 13, color: Colors.grey,),
                              Text(" ATUALIZADO HÁ " + FuncoesExtras.getTempoAtualizado(snapshot.dataPreco), style: TextStyle(fontSize: 13.0, fontWeight: FontWeight.w300), textAlign: TextAlign.start,),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  !isCombustivel ? Container(
                    height: 50,
                    width: 40,
                    decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.only(topLeft: const Radius.circular(15.0), bottomLeft: const Radius.circular(15.0)),
                      ),
                      child: Center(
                        child: IconButton(
                          icon: Icon(Icons.add_shopping_cart, color: Colors.white), 
                          onPressed: func,
                        ),
                      ),
                  ) : Container()
                ],
              ),
            ),
            onTap: (){
              if(!isCombustivel) {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (context)=>ProdutoScreen(produto: snapshot))
                );
              } else {
                _showModalIrParaPosto(snapshot, context);
              }
            },
    );
  }

  Future _showModalIrParaPosto(Produto produto, BuildContext context) async {
    switch(
      await showDialog(
        context: context,
        child: new SimpleDialog(
          backgroundColor: Colors.yellow,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
          title: new Text(produto.empresa,
            style: TextStyle(
              color: Colors.black87, 
              fontSize: 16, ), 
              textAlign: TextAlign.center,),
          children: <Widget>[
            new SimpleDialogOption(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Icon(FontAwesomeIcons.compass, color: Colors.red,),
                      Text( " Ir até o Posto", 
                        style: TextStyle(
                          color: Colors.red, 
                          fontWeight: FontWeight.bold,
                          fontSize: 25), 
                          textAlign: TextAlign.center,),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Text("R\$ ", style: TextStyle(color: Colors.red, fontSize: 10),),
                      Text( double.parse(produto.preco).toStringAsFixed(2), 
                        style: TextStyle(
                          color: Colors.red,
                          fontWeight: FontWeight.bold, 
                          fontSize: 22), 
                          textAlign: TextAlign.center,),
                    ],
                  ),
                ],
              ),
              onPressed: (){
                Navigator.pop(context, "SIM");
              },
            ),
          ],
        )
      )
    ) {
      case "SIM":
        FuncoesExtras.abrirMaps(produto);
        break;
    }
  }
}