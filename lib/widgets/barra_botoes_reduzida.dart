import 'package:economizarr/screens/listacompras_screen.dart';
import 'package:economizarr/screens/meusfiltros_screen.dart';
import 'package:economizarr/utils/enum_cobustivel.dart';
import 'package:economizarr/utils/funcoes_extras.dart';
import 'package:economizarr/widgets/button_hide_show.dart';
import 'package:economizarr/widgets/button_navigation_sheet.dart';
import 'package:economizarr/widgets/produto_scanning_codebar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class BarraBotoesReduzida extends StatelessWidget {

  final Function func;
  final bool stateSheet;

  BarraBotoesReduzida({@required this.func, @required this.stateSheet});

  @override
  Widget build(BuildContext context) {

    void _irParaListaCompras() {
      Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => ListaComprasScreen()
        ));
    }

    void _irParaMeusFiltros(){
      Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => MeusFiltrosScreen()
        ));
    }

    void _irParaCapturaCodeBar() async {
      String barcodeScanRes = await FuncoesExtras.scanningCodeBar();
      if(barcodeScanRes != "-1")
        FuncoesExtras.pesquisar(barcodeScanRes, context, false, true);
    }

   Future _showModalCombustivel() async {
    switch(
      await showDialog(
        context: context,
        child: new SimpleDialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
          title: new Text("Combustíveis", 
            style: TextStyle(
              color: Theme.of(context).dividerColor, 
              fontSize: 22, 
              fontWeight: FontWeight.bold), 
              textAlign: TextAlign.center,),
          children: <Widget>[
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Divider(
                thickness: 2.0,
                color: Theme.of(context).dividerColor,
              ),
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                new SimpleDialogOption(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Text( "GASOLINA", 
                        style: TextStyle(
                          color: Theme.of(context).accentColor, 
                          fontSize: 20), 
                          textAlign: TextAlign.center,),
                      Icon(Icons.arrow_forward_ios, color: Theme.of(context).accentColor,)
                    ],
                  ),
                  onPressed: (){
                    Navigator.pop(context, Combustivel.GASOLINA);
                  },
                ),
                Divider(
                  indent: 10,
                  endIndent: 10,
                  color: Theme.of(context).dividerColor,
                ),
                new SimpleDialogOption(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Text( "ETANOL", 
                        style: TextStyle(
                          color: Theme.of(context).accentColor, 
                          fontSize: 20), 
                          textAlign: TextAlign.center,),
                      Icon(Icons.arrow_forward_ios, color: Theme.of(context).accentColor,)
                    ],
                  ),
                  onPressed: (){
                    Navigator.pop(context, Combustivel.ETANOL);
                  },
                ),
                 Divider(
                  indent: 10,
                  endIndent: 10,
                  color: Theme.of(context).dividerColor,
                ),
                new SimpleDialogOption(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Text( "DIESEL", 
                        style: TextStyle(
                          color: Theme.of(context).accentColor, 
                          fontSize: 20), 
                          textAlign: TextAlign.center,),
                      Icon(Icons.arrow_forward_ios, color: Theme.of(context).accentColor,)
                    ],
                  ),
                  onPressed: (){
                    Navigator.pop(context, Combustivel.DIESEL);
                  },
                ),
                 Divider(
                  indent: 10,
                  endIndent: 10,
                  color: Theme.of(context).dividerColor,
                ),
                new SimpleDialogOption(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Text( "GNV", 
                        style: TextStyle(
                          color: Theme.of(context).accentColor, 
                          fontSize: 20), 
                          textAlign: TextAlign.center,),
                      Icon(Icons.arrow_forward_ios, color: Theme.of(context).accentColor,)
                ],
              ),
              onPressed: (){
                Navigator.pop(context, Combustivel.GNV);
              },
            ),
             Divider(
                  indent: 10,
                  endIndent: 10,
                  color: Theme.of(context).dividerColor,
                ),
              ],
            )
          ],
        )
      )
    ) {
      case Combustivel.GASOLINA:
        FuncoesExtras.pesquisar("GASOLINA", context, true, false);
        break;
      case Combustivel.DIESEL:
        FuncoesExtras.pesquisar("DIESEL", context, true, false);
        break;
      case Combustivel.ETANOL:
        FuncoesExtras.pesquisar("ETANOL", context, true, false);
        break;
      case Combustivel.GNV:
        FuncoesExtras.pesquisar("GNV", context, true, false);
        break;
    }
  }

    return BottomAppBar(
              color: Colors.transparent,
              child: Container(
                decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(topLeft: const Radius.circular(15.0), topRight: const Radius.circular(15.0)),
                      ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ButtonHideShow(func: func),
                  Visibility(visible: stateSheet, child: ScanearCodeBarProduto()),
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(topLeft: const Radius.circular(15.0), topRight: const Radius.circular(15.0)),
                      ),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: stateSheet ? MainAxisAlignment.spaceBetween : MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                            Visibility(visible: !stateSheet, child: ButtonNavigationSheet(stateSheet: stateSheet, iconData: FontAwesomeIcons.barcode, func: _irParaCapturaCodeBar, title: "ESCANEAR CODIGO DE BARRAS", context: context)),
                            ButtonNavigationSheet(stateSheet: stateSheet, iconData: FontAwesomeIcons.gasPump, func: _showModalCombustivel, title: "PREÇO DO COMBUSTÍVEL", context: context),
                            ButtonNavigationSheet(stateSheet: stateSheet, iconData: FontAwesomeIcons.clipboardList, func: _irParaListaCompras, title: "LISTA DE COMPRAS", context: context),
                            ButtonNavigationSheet(stateSheet: stateSheet, iconData: FontAwesomeIcons.cog, func: _irParaMeusFiltros, title: "MEUS FILTROS", context: context),
                      ],
                    ),
                  ),
                ],
              ),
            ),
      );
  }
}