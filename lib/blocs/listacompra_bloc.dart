import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:economizarr/database/database.dart';
import 'package:economizarr/model/listacompra_model.dart';
import 'package:economizarr/model/produto_model.dart';

class ListaCompraBloc implements BlocBase {

  List<ListaCompra> produtos = List();

  StreamController<List<ListaCompra>> _controller;
  Stream get outLista => _controller.stream;

  Sink get inLista => _controller.sink;

  bool _isDisposed = false;

  ListaCompraBloc(){
    _controller = StreamController<List<ListaCompra>>();
  }

  getListaCompras() async {

    if(_isDisposed)
      _controller = StreamController<List<ListaCompra>>();

    inLista.add(await DBProvider.db.getAllListCompras());

  }

  update(ListaCompra listaCompra) {
    DBProvider.db.update(listaCompra);
    getListaCompras();
  }

  delete(int id) async {
    await DBProvider.db.deleteProdutosByLista(id);
    await DBProvider.db.delete(id);
    getListaCompras();
  }

  insert(ListaCompra listaCompra) {
    DBProvider.db.insert(listaCompra);
    getListaCompras();
  }

  addToList(Produto produto, ListaCompra lista){
    // Produto pMap = Produto.fromMap(json)
    print(produto.produto + ' na lista ' + lista.nome);
    Produto prod = Produto.addListMap(produto, lista);
    DBProvider.db.insertProduto(prod);
  }

  @override
  void addListener(listener) {
  }

  @override
  void dispose() {
    _controller.close();
    _isDisposed = true;
  }

  @override
  bool get hasListeners => null;

  @override
  void notifyListeners() {
  }

  @override
  void removeListener(listener) {
  }

}