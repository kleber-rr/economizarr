import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:economizarr/database/database.dart';
import 'package:economizarr/model/produto_model.dart';

class ListaProdutoBloc implements BlocBase {

  List<Produto> produtos = List();

  StreamController<List<Produto>> _controller;
  Stream get outLista => _controller.stream;

  Sink get inLista => _controller.sink;

  bool _isDisposed = false;

  ListaProdutoBloc(){
    _controller = StreamController<List<Produto>>();
  }

  getProdutos(int id) async {

    if(_isDisposed)
      _controller = StreamController<List<Produto>>();

    inLista.add(await DBProvider.db.getAllProdutosPorListaCompra(id));

  }

  update(Produto produto) async {
    DBProvider.db.updateProduto(produto);
    inLista.add(await DBProvider.db.getAllProdutosPorListaCompra(produto.lista));
  }

  delete(Produto produto) {
    int id = produto.id;
    DBProvider.db.deleteProduto(id);
    getProdutos(produto.lista);
  }

  insert(Produto produto) {
    DBProvider.db.insertProduto(produto);
    getProdutos(produto.lista);
  }

  @override
  void addListener(listener) {
  }

  @override
  void dispose() {
    _controller.close();
    _isDisposed = true;
  }

  @override
  bool get hasListeners => null;

  @override
  void notifyListeners() {
  }

  @override
  void removeListener(listener) {
  }

}