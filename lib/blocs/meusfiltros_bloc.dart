import 'dart:async';
import 'dart:convert';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:economizarr/model/prefs_model.dart';
import 'package:economizarr/utils/funcoes_extras.dart';
import 'package:rxdart/subjects.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MeusFiltrosBloc implements BlocBase {

static Preferences preferencias;

  var _prefsController = BehaviorSubject<Preferences>.seeded(preferencias);
  Stream<Preferences> get outPrefs => _prefsController.stream;

  bool _isDisposed = false;

  MeusFiltrosBloc(){
      
    SharedPreferences.getInstance().then((prefs){
      if(prefs.get(FuncoesExtras.SHARED_PREFS_NAME) == null){
        defaultPreferences();
      } else {
        final jsonResponse = json.decode(prefs.get(FuncoesExtras.SHARED_PREFS_NAME));
        preferencias = new Preferences.fromJson(jsonResponse);

      if(preferencias == null){
        defaultPreferences();
      } else {
        _prefsController.sink.add(preferencias);
      }
      }
    });

    

  }

  void defaultPreferences(){
    Preferences defaultPrefs = Preferences.fromJson(
          {
            "id":"1",
            "usarLocalizacao": true,
            "municipio": "BOA VISTA",
            "raioBusca": 30.0,
            "filtroTempo": 20.0
          }
        );
    tooglePreferences(defaultPrefs);
  }

void tooglePreferences(Preferences preferences){
  if(_isDisposed)
  _prefsController = BehaviorSubject<Preferences>.seeded(preferencias);

  preferencias = preferences;
  _prefsController.sink.add(preferences);

  _saveFav();
}

void _saveFav(){
  SharedPreferences.getInstance().then((prefs){
    prefs.setString(FuncoesExtras.SHARED_PREFS_NAME, json.encode(preferencias));
  });
}

  @override
  void dispose() {
    _prefsController.close();
    _isDisposed = true;
  }

  @override
  void addListener(listener) {
  }

  @override
  bool get hasListeners => null;

  @override
  void notifyListeners() {
  }

  @override
  void removeListener(listener) {
  }

}