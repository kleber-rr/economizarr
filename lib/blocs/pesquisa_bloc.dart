import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:economizarr/api.dart';
import 'package:economizarr/model/produto_model.dart';

class PesquisaBloc implements BlocBase {

  List<Produto> listaTodosProdutos = List();
  List<Produto> listaProdutosExibidos = List();
  int qtdeExibidaPorRequisicao = 10;
  int ultimoExibido = 0;
  int totalResultados = 0;

  bool isDisposed = false;

  Api api;

  StreamController<List<Produto>> _controller;
  Stream get outLista => _controller.stream.asBroadcastStream();

  Sink get inLista => _controller.sink;

  StreamController<Map<String, dynamic>> _controllerSearch;
  Stream get outSearch => _controllerSearch.stream;

  Sink get inSearch => _controllerSearch.sink;

  PesquisaBloc(){
    initListeners();
        
      }
    
      @override
      void dispose() {
        _controller.close();
        _controllerSearch.close();
        isDisposed = true;
      }
    
      @override
      bool get hasListeners => (inSearch == null);
    
      @override
      void notifyListeners() {
      }
    
      @override
      void removeListener(listener) {
      }
    
      @override
      void addListener(listener) {
      }
    
      void initListeners() {
        listaTodosProdutos.clear();
        listaProdutosExibidos.clear();
        ultimoExibido = 0;
        totalResultados = 0;
        _controller = StreamController<List<Produto>>.broadcast();
        _controllerSearch = StreamController<Map<String, dynamic>>();
        isDisposed = false;

        outSearch.listen((data){
          Map<String, dynamic> mapa = data;
            api = new Api(mapa['params'], mapa['city'], mapa['radius'], mapa['latlng'], mapa['diasRetro']);
    
            api.search().then((data){
              listaTodosProdutos.addAll(data);
              totalResultados = listaTodosProdutos.length;
              int range = listaTodosProdutos.length > qtdeExibidaPorRequisicao ? qtdeExibidaPorRequisicao : listaTodosProdutos.length;
              
              atualizarLista(range);
            });
        });
      }

      void atualizarLista(int range) {
        listaProdutosExibidos.addAll(listaTodosProdutos.getRange(ultimoExibido, (ultimoExibido + range)));
        inLista.add(listaProdutosExibidos);
        ultimoExibido = ultimoExibido + range;
      }

      Future<bool> nextPage() async {
        
        if(ultimoExibido >= totalResultados){
          api.nextPage().then((data){
            listaTodosProdutos.addAll(data);
            totalResultados = listaTodosProdutos.length;
            int range = (listaTodosProdutos.length - listaProdutosExibidos.length) > qtdeExibidaPorRequisicao ? qtdeExibidaPorRequisicao : (listaTodosProdutos.length - listaProdutosExibidos.length);
            
            atualizarLista(range);
          });
        } else {
          int range = (listaTodosProdutos.length - listaProdutosExibidos.length) > qtdeExibidaPorRequisicao ? qtdeExibidaPorRequisicao : (listaTodosProdutos.length - listaProdutosExibidos.length);

          atualizarLista(range);
        }
        return true;
      }
}