import 'package:economizarr/model/listacompra_model.dart';

class Produto {
  final String rn;
  final String doc;
  final String empresa;
  final String logra;
  final String endNum;
  final String bairro;
  final String municipio;
  final String uf;
  final String produto;
  final String preco;
  final String dataPreco;
  final String latitude;
  final String longitude;
  final double distanciaKm;
  final int lista;
  final int id;
  int qtdeItens;

  Produto({this.rn, this.doc, this.empresa, this.logra, 
    this.endNum, this.bairro, this.municipio, this.uf, 
    this.produto, this.preco, this.dataPreco, this.lista, 
    this.qtdeItens, this.id, this.latitude, this.longitude,
    this.distanciaKm});

  factory Produto.fromJson(Map<String, dynamic> json){
      return Produto(
        rn: json["rn"],
        doc: json["doc"],
        empresa: json["empresa"],
        logra: json["logra"],
        endNum: json["end_num"],
        bairro: json["bairro"],
        municipio: json["municipio"],
        uf: json["uf"],
        produto: json["produto"],
        preco: json["preco"],
        dataPreco: json["data_preco"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        distanciaKm: json["distancia_km"]
      );
  }

  Map<String, dynamic> toJson(){
    return {
      "rn":rn,
      "doc":doc,
      "empresa":empresa,
      "logra":logra,
      "end_num":endNum,
      "bairro":bairro,
      "municipio":municipio,
      "uf":uf,
      "produto":produto,
      "preco":preco,
      "data_preco":dataPreco,
      "latitude": latitude,
      "longitude": longitude,
      "distancia_km": distanciaKm
    };
  }

    factory Produto.fromMap(Map<String, dynamic> json){
      return Produto(
        rn: json["rn"],
        doc: json["doc"],
        empresa: json["empresa"],
        logra: json["logra"],
        endNum: json["end_num"],
        bairro: json["bairro"],
        municipio: json["municipio"],
        uf: json["uf"],
        produto: json["produto"],
        preco: json["preco"],
        dataPreco: json["data_preco"],
        lista: json["lista_id"],
        qtdeItens: json["qtde_itens"],
        id: json["id"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        distanciaKm: json["distancia_km"]
      );
  }

  Map<String, dynamic> toMap(){
    return {
      "rn":rn,
      "doc":doc,
      "empresa":empresa,
      "logra":logra,
      "end_num":endNum,
      "bairro":bairro,
      "municipio":municipio,
      "uf":uf,
      "produto":produto,
      "preco":preco,
      "data_preco":dataPreco,
      "lista_id": lista,
      "qtde_itens": qtdeItens,
      "id":id,
      "latitude": latitude,
      "longitude": longitude,
      "distancia_km": distanciaKm
    };
  }

  factory Produto.addListMap(Produto prd, ListaCompra lista){
      return Produto(
        rn: prd.rn,
        doc: prd.doc,
        empresa: prd.empresa,
        logra: prd.logra,
        endNum: prd.endNum,
        bairro: prd.bairro,
        municipio: prd.municipio,
        uf: prd.uf,
        produto: prd.produto,
        preco: prd.preco,
        dataPreco: prd.dataPreco,
        lista: lista.id,
        qtdeItens: 1,
        latitude: prd.latitude,
        longitude: prd.longitude,
        distanciaKm: prd.distanciaKm
      );
  }

}