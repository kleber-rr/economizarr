import 'package:economizarr/model/produto_model.dart';

class ListaCompra {
  final int id;
  final String nome;
  List<Produto> itensCompra;

  ListaCompra({this.id, this.nome, this.itensCompra});

  factory ListaCompra.fromMap(Map<String, dynamic> json){
      return ListaCompra(
        id: json["id"],
        nome: json["nome"]
      );
  }

  Map<String, dynamic> toMap(){
    return {
      "id":id,
      "nome":nome
    };
  }
}