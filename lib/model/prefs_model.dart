class Preferences {
  final String id;
  final bool usarLocalizacao;
  final String municipio;
  final double raioBusca;
  final double filtroTempo;

  Preferences({this.id, this.usarLocalizacao, this.municipio, this.raioBusca, this.filtroTempo});

  factory Preferences.fromJson(Map<String, dynamic> json){
      return Preferences(
        id: json["id"],
        usarLocalizacao: json["usarLocalizacao"],
        municipio: json["municipio"],
        raioBusca: json["raioBusca"],
        filtroTempo: json["filtroTempo"]
      );
  }

  Map<String, dynamic> toJson(){
    return {
      "id": id,
      "usarLocalizacao": usarLocalizacao,
      "municipio": municipio,
      "raioBusca":raioBusca,
      "filtroTempo": filtroTempo
    };
  }
}