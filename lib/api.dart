import 'dart:convert';
// import 'package:bloc_pattern/bloc_pattern.dart';
// import 'package:economizarr/blocs/pesquisa_bloc.dart';
import 'package:economizarr/model/produto_model.dart';
import 'package:http/http.dart' as http;

const API_URL = "http://189.86.30.9/apipreco/public/api/v1/precos?";

class Api {

  String _search;
  String _limit = '100';
  String _offSet = '0';
  String _diasRetro;
  String _city;
  String _radius;
  String _latlng;
  // PesquisaBloc blocSearch;

  Api(String params, String city, String radius, String latlng, String diasRetro){
    _search = params.toUpperCase();
    _city = city == null || city.isEmpty ? "BOA_VISTA" : city.toUpperCase().replaceAll(" ", "_");
    _radius = radius == null || radius.isEmpty ? "0" : radius;
    _latlng = latlng == null || latlng.isEmpty ? "0" : latlng;
    _diasRetro = diasRetro == null || diasRetro.isEmpty ? "7" : diasRetro;

    //  blocSearch = BlocProvider.getBloc<PesquisaBloc>();
  }

  Future<List<Produto>> search() async {

    String url = API_URL + "l=$_limit&s=$_offSet&d=$_diasRetro&p=$_search&m=$_city&g=$_latlng&r=$_radius";
    print(url);

    http.Response response = await http.get(url);

    return decode(response);
  }

  List<Produto> decode(http.Response response){
    if(response.statusCode == 200){
      List<dynamic> produtoMap = jsonDecode(response.body);

      List<Produto> produtos = produtoMap.map<Produto>(
        (map){
          return Produto.fromJson(map);
        }
      ).toList();

      return produtos;
    } else {
      throw Exception("Falhou ao carregar produtos");
    }
  }

  Future<List<Produto>> nextPage() async {
    _offSet = (int.parse(_offSet) + 101).toString();

    String url = API_URL + "l=$_limit&s=$_offSet&d=$_diasRetro&p=$_search&m=$_city&g=$_latlng&r=$_radius";
    
    http.Response response = await http.get(url);

    print(url);

    return decode(response);
  }

}