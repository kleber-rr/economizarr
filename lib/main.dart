import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:economizarr/blocs/listacompra_bloc.dart';
import 'package:economizarr/blocs/listaproduto_bloc.dart';
import 'package:economizarr/blocs/meusfiltros_bloc.dart';
import 'package:economizarr/blocs/pesquisa_bloc.dart';
import 'package:economizarr/screens/splash_screen.dart';
import 'package:economizarr/utils/funcoes_extras.dart';
import 'package:flutter/material.dart';
// import 'package:map_view/map_view.dart';

void main() {
  // MapView.setApiKey(FuncoesExtras.API_KEY);
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      blocs: [
        Bloc((i) => ListaCompraBloc()),
        Bloc((i) => ListaProdutoBloc()),
        Bloc((i) => MeusFiltrosBloc()),
        Bloc((i) => PesquisaBloc())
        ],
      child: MaterialApp(
        title: 'EconomizaRR',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: FuncoesExtras.hexToColor('#005494'),
          accentColor: Colors.lightBlue[300],
          dividerColor: Color.fromRGBO(0, 126, 218, 1.0),
          bottomSheetTheme: BottomSheetThemeData(
              backgroundColor: Colors.white.withOpacity(0)),
        ),
        home: SplashScreen(),
      ),
    );
  }
}

