import 'package:economizarr/model/listacompra_model.dart';
import 'package:economizarr/model/produto_model.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

const int VERSAO_DB = 1;
const String NOME_BD = "economizarr_database.db";
const String LISTA_COMPRA = "listacompra";
const String PRODUTO = "produto";

class DBProvider {
  DBProvider._();
  static final DBProvider db = DBProvider._();

  static Database _database;

  Future<Database> get database async {
    if (_database != null)
    return _database;

    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    return openDatabase(
      // Set the path to the database. Note: Using the `join` function from the
      // `path` package is best practice to ensure the path is correctly
      // constructed for each platform.
      join(await getDatabasesPath(), NOME_BD), 
      // onOpen: (db){
      //   db.getVersion().then((old) {
      //     if(old != VERSAO_DB){
      //       db.delete(LISTA_COMPRA);
      //     }
      //   });},
      onCreate: (db, version) {
        _create(db, version);
      },
      // Set the version. This executes the onCreate function and provides a
      // path to perform database upgrades and downgrades.
      version: VERSAO_DB,
    );
  }

  Future _create(Database db, int version) async {
    await db.execute(
            "CREATE TABLE "+LISTA_COMPRA+"( "+
             "id INTEGER PRIMARY KEY, nome TEXT)"
            );

    await db.execute(
            "CREATE TABLE "+PRODUTO+"( "+
              "id INTEGER PRIMARY KEY, "+
              "rn TEXT, "+
              "doc TEXT, "+
              "empresa TEXT, "+
              "logra TEXT, "+
              "end_num TEXT, "+
              "bairro TEXT, "+
              "municipio TEXT,"+ 
              "uf TEXT, "+
              "produto TEXT, "+
              "preco TEXT, "+
              "data_preco TEXT, "+
              "lista_id INTEGER NOT NULL, "+
              "qtde_itens INTEGER, "+
              "latitude TEXT, " +
              "longitude TEXT, " +
              "distancia_km DOUBLE, " +
              "FOREIGN KEY (lista_id) REFERENCES "+LISTA_COMPRA +" (id) "+ 
              "ON DELETE NO ACTION ON UPDATE NO ACTION)");
  }

  getAllListCompras() async {
    final db = await database;
    var res = await db.query(LISTA_COMPRA);
    List<ListaCompra> list =
        res.isNotEmpty ? res.map((c) => ListaCompra.fromMap(c)).toList() : [];
    for(ListaCompra item in list){
      List<Produto> listaProd = await getAllProdutosPorListaCompra(item.id);
      item.itensCompra = listaProd;
    }
    return list;
  }

  update(ListaCompra listaCompra) async {
    final db = await database;
    var res = await db.update(LISTA_COMPRA, listaCompra.toMap(),
        where: "id = ?", whereArgs: [listaCompra.id]);
    return res;
  }

  delete(int id) async {
    final db = await database;
    db.delete(LISTA_COMPRA, where: "id = ?", whereArgs: [id]);
  }

  deleteAll() async {
    final db = await database;
    db.rawDelete("Delete * from "+LISTA_COMPRA+"");
  }

  insert(ListaCompra listaCompra) async {
    final db = await database;
    var res = await db.insert(LISTA_COMPRA, listaCompra.toMap());
    return res;
  }
  
  insertProduto(Produto produto) async {
    final db = await database;
    var res = await db.insert(PRODUTO, produto.toMap());
    return res;
  }
  
  deleteProduto(int id) async {
    final db = await database;
    db.delete(PRODUTO, where: "id = ?", whereArgs: [id]);
  }
  
  deleteProdutosByLista(int id) async {
    final db = await database;
    db.delete(PRODUTO, where: "lista_id = ?", whereArgs: [id]);
  }

   getAllProdutosPorListaCompra(int idLista) async {
    final db = await database;
    var res = await db.query(PRODUTO, where: "lista_id = ?", whereArgs: [idLista]);
    List<Produto> list =
        res.isNotEmpty ? res.map((c) => Produto.fromMap(c)).toList() : [];
    return list;
  }

  updateProduto(Produto produto) async {
    final db = await database;
    var res = await db.update(PRODUTO, produto.toMap(),
        where: "id = ?", whereArgs: [produto.id]);
    return res;
  }

}