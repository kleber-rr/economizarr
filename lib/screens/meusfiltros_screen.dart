import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:economizarr/blocs/meusfiltros_bloc.dart';
import 'package:economizarr/model/prefs_model.dart';
import 'package:economizarr/utils/funcoes_extras.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MeusFiltrosScreen extends StatefulWidget {
  @override
  _MeusFiltrosScreenState createState() => _MeusFiltrosScreenState();
}

class _MeusFiltrosScreenState extends State<MeusFiltrosScreen> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final blocPrefs = BlocProvider.getBloc<MeusFiltrosBloc>();

  bool minhaLocalizacao = false;
  String dropdownValue = "BOA VISTA";
  double raioBusca = 10;
  String _labelRaio = "Distância: ";
  double raioTempo = 10;
  String _labelTempo = "Atualizado em: ";
  Preferences _prefs;

  _MeusFiltrosScreenState(){
    blocPrefs.outPrefs.listen((data){
      _prefs = data;
      if(_prefs != null)
        setState(() {
          minhaLocalizacao = _prefs.usarLocalizacao;
          dropdownValue = _prefs.municipio;
          raioBusca = _prefs.raioBusca;
          raioTempo = _prefs.filtroTempo;
          _labelRaio = FuncoesExtras.raioBusca(raioBusca.toString());
          _labelTempo = FuncoesExtras.raioTempo(raioTempo.toString());
        });
    });
  }

  @override
  void dispose() {
    blocPrefs.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.lightBlue[300],
      appBar: FuncoesExtras.getTitleAppBar(context, false),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: ListView(
          children: <Widget>[
            Container(
              height: 340,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15.0)),
                margin: EdgeInsets.symmetric(horizontal: 16, vertical: 5),
                padding: EdgeInsets.all(27),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Flexible(
                      child: Text("FILTROS DE LOCALIZAÇÃO", style: TextStyle(color: Theme.of(context).accentColor, fontSize: 18, fontWeight: FontWeight.bold),),
                    ),
                    Divider(color: Theme.of(context).primaryColor,),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                              child: RichText(
                                  text: TextSpan(
                                    text: 'Use a minha localização',
                                    style: TextStyle(color: Theme.of(context).accentColor, fontSize: 16),
                                    children: <TextSpan>[
                                      TextSpan(
                                          text: '\nProcura os menores preços próximos a você',
                                          style: TextStyle(color: Theme.of(context).accentColor, fontSize: 12)),
                                    ],
                                  ),
                                )
                            ),
                            Switch(
                              value: minhaLocalizacao,
                              onChanged: (val){
                                setState(() {
                                  minhaLocalizacao = val;
                                });
                              },
                            )
                      ],
                    ),
                    Container(height: 10,),
                    Flexible(
                      child: RichText(
                          text: TextSpan(
                            text: 'Busca por Município',
                            style: TextStyle(color: Theme.of(context).accentColor, fontSize: 16),
                            children: <TextSpan>[
                              TextSpan(
                                  text: '\nEscolha uma cidade específica para a busca',
                                  style: TextStyle(color: Theme.of(context).accentColor, fontSize: 12)),
                            ],
                          ),
                        )
                    ),
                  Container(height: 10,),
                  Center(
                    child: Container(
                      padding: EdgeInsets.all(8),
                      decoration: ShapeDecoration(
                        shape: RoundedRectangleBorder(
                          side: BorderSide(width: 1.0, style: BorderStyle.solid,color: Theme.of(context).accentColor),
                          borderRadius: BorderRadius.all(Radius.circular(1.0)),
                        ),
                      ),
                      child: DropdownButton<String>(
                        value: dropdownValue,
                        icon: Icon(FontAwesomeIcons.chevronDown, color: Theme.of(context).primaryColor,),
                        iconSize: 15,
                        elevation: 16,
                        isDense: true,
                        style: TextStyle(
                          color: Theme.of(context).primaryColor
                        ),
                        underline: Container(
                          height: 0,
                          color: Colors.transparent,
                        ),
                        onChanged: (String newValue) {
                          setState(() {
                            dropdownValue = newValue;
                          });
                        },
                        items: <String>['ESCOLHA UM MUNICÍPIO', 'ALTO ALEGRE','AMAJARI','BOA VISTA','BONFIM','CANTÁ','CARACARAÍ','CAROEBE','IRACEMA','MUCAJAÍ','NORMANDIA','PACARAIMA','RORAINÓPOLIS','SÃO JOÃO DA BALIZA','SÃO LUÍS','UIRAMUTÃ',]
                          .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          })
                          .toList(),
                      ),
                    ),
                  ),
                Container(height: 10,),
                  Flexible(
                    child: RichText(
                          text: TextSpan(
                            text: 'Raio de Busca\n',
                            style: TextStyle(color: Theme.of(context).accentColor, fontSize: 16),
                            children: <TextSpan>[
                              TextSpan(
                                  text: _labelRaio,
                                  style: TextStyle(color: Theme.of(context).accentColor, fontSize: 12)),
                            ],
                          ),
                        )
                  ),
                Container(height: 10,),
                Slider(
                  label: _labelRaio,
                  min: 0.0,
                  max: 30.0,
                  divisions: 3,
                  onChanged: (val){
                    setState(() {
                      raioBusca = val;
                      _labelRaio = FuncoesExtras.raioBusca(raioBusca.toString());
                      print(_labelRaio);
                    });
                  },
                  value: raioBusca,
                )
                  ],
                ),
              ),
            Container(
              height: 200,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15.0)),
                margin: EdgeInsets.symmetric(horizontal: 16, vertical: 5),
                padding: EdgeInsets.all(27),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Flexible(
                      child: Text("FILTROS DE TEMPO", style: TextStyle(color: Theme.of(context).accentColor, fontSize: 18, fontWeight: FontWeight.bold),),
                    ),
                    Divider(color: Theme.of(context).primaryColor,),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                              child: RichText(
                                  text: TextSpan(
                                    text: 'Define o filtro de tempo desde a última venda\n',
                                    style: TextStyle(color: Theme.of(context).accentColor, fontSize: 16),
                                    children: <TextSpan>[
                                      TextSpan(
                                          text: _labelTempo,
                                          style: TextStyle(color: Theme.of(context).accentColor, fontSize: 12)),
                                    ],
                                  ),
                                )
                            ),
                      ],
                ),
                Container(height: 10,),
                Slider(
                  label: _labelTempo,
                  min: 0.0,
                  max: 20.0,
                  divisions: 2,
                  onChanged: (val){
                    setState(() {
                      raioTempo = val;
                      _labelTempo = FuncoesExtras.raioTempo(raioTempo.toString());
                      print(_labelTempo);
                    });
                  },
                  value: raioTempo,
                )
                  ],
                ),
              ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 16, vertical: 5),
              child: RaisedButton(
                onPressed: () {
                  Preferences newPrefs = Preferences.fromJson(
                    {
                      "id":"1",
                      "usarLocalizacao": minhaLocalizacao,
                      "municipio": dropdownValue,
                      "raioBusca": raioBusca,
                      "filtroTempo": raioTempo
                    }
                  );
                  //verifica se ouve mudança
                  if(_prefs != newPrefs){
                    blocPrefs.tooglePreferences(newPrefs);
                    final snackBar = SnackBar(
                      content: Text('Salvo com sucesso'),
                    );

                    (_scaffoldKey).currentState.showSnackBar(snackBar);
                  }
                },
                padding: EdgeInsets.all(16),
                color: Colors.orange[600],
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)),
                child: Text("Salvar", style: TextStyle(color: Colors.white, fontSize: 18),),
              ),
            ),

            Container(
              margin: EdgeInsets.symmetric(horizontal: 16, vertical: 5),
              child: RaisedButton(
                onPressed: () {
                  blocPrefs.defaultPreferences();
                  final snackBar = SnackBar(
                      content: Text('Configurações iniciais restauradas'),
                    );

                    (_scaffoldKey).currentState.showSnackBar(snackBar);
                },
                padding: EdgeInsets.all(16),
                color: Colors.orange[600],
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)),
                child: Text("Confirgurações Iniciais", style: TextStyle(color: Colors.white, fontSize: 18),),
              ),
            ),
          ],
        ),
      ),
    );
  }
}