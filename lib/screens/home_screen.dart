import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:economizarr/blocs/meusfiltros_bloc.dart';
import 'package:economizarr/utils/funcoes_extras.dart';
import 'package:economizarr/widgets/barra_botoes_reduzida.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  bool _stateSheet = true;

  final pesquisaController = TextEditingController();

  final blocPrefs = BlocProvider.getBloc<MeusFiltrosBloc>();

  // void _pesquisar(String text) {
  //   if(text.isNotEmpty){
  //     Navigator.of(context).push(MaterialPageRoute(
  //       builder: (context) => PesquisaScreen(params: text, isCombustivel: false,)
  //     ));
  //   }
  // }

  @override
  void dispose() {
    blocPrefs.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

  // final blocSearch = BlocProvider.getBloc<PesquisaBloc>();

    return Scaffold(
      backgroundColor: Colors.lightBlue[300],
      appBar: FuncoesExtras.getTitleAppBar(context, true),
      body: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
                height: 70.0,
                margin: const EdgeInsets.all(15),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius:
                      new BorderRadius.all(const Radius.circular(30.0)),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: buildTextField(
                        "Pesquisar pelo nome do produto",
                        pesquisaController,
                        FontAwesomeIcons.searchDollar),
                  ),
                )
              ),
            //
            // BoxInferior()
          ]
          ),
      bottomNavigationBar: BarraBotoesReduzida(func: (){
            setState(() {
              _stateSheet = !_stateSheet; 
            });
          }, stateSheet: _stateSheet,),
    );
  }

  Widget buildTextField(String label, TextEditingController controller,
      IconData icon) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextField(
        controller: controller,
        textAlign: TextAlign.center,
        textAlignVertical: TextAlignVertical.center,
        decoration: new InputDecoration(
            labelText: label,
            border: InputBorder.none,
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white, width: 32.0),
                    borderRadius: BorderRadius.circular(25.0)),
            labelStyle: TextStyle(color: Theme.of(context).primaryColor, fontSize: 15.0, fontWeight: FontWeight.normal,),
            prefixIcon: Icon(
              icon,
              size: 30.0,
              color: Theme.of(context).primaryColor,
            )),
        style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 25.0, fontWeight: FontWeight.bold,),
        onTap: () {
          setState(() {
            _stateSheet = false;
          });
        },
        onSubmitted: (val) async {
          controller.text = "";
          FuncoesExtras.pesquisar(val, context, false, false);
        },
      ),
    );
  }
}