import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:economizarr/blocs/listacompra_bloc.dart';
import 'package:economizarr/blocs/meusfiltros_bloc.dart';
import 'package:economizarr/blocs/pesquisa_bloc.dart';
import 'package:economizarr/model/produto_model.dart';
import 'package:economizarr/utils/funcoes_extras.dart';
import 'package:economizarr/widgets/barra_botoes_reduzida.dart';
import 'package:economizarr/widgets/produto_tile.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:location/location.dart';

class PesquisaScreen extends StatefulWidget {
  final String params;
  final bool isCombustivel;

  PesquisaScreen({Key key, @required this.params, @required this.isCombustivel}) : super(key: key);
  @override
  _PesquisaScreenState createState() => _PesquisaScreenState(params, isCombustivel);
}

class _PesquisaScreenState extends State<PesquisaScreen> {

  final blocLista = BlocProvider.getBloc<ListaCompraBloc>();
  final blocPrefs = BlocProvider.getBloc<MeusFiltrosBloc>();
  final blocSearch = BlocProvider.getBloc<PesquisaBloc>();

  String _params;
  bool _isCombustivel;
  String _city = "";
  String _radius = "";
  String _latlng = "";
  String _diasRetro = "";

  // Api api;

  List<Produto> items;
  ScrollController _scrollController = new ScrollController();
  bool isPerformingRequest = false;

  _PesquisaScreenState(String params, bool isCombustivel){
    _params = params;
    _isCombustivel = isCombustivel == null ? false : isCombustivel;
    items = new List();
  }

  @override
  void initState() {
    super.initState();

    setState(() => isPerformingRequest = true);

    blocPrefs.outPrefs.listen((data) {
     
      try{
        //aguarda as preferencias
        if(data != null){
        
        //verifica a localizacao

          Location location = new Location();
            location.getLocation().then((onValue){

              if(data.usarLocalizacao){
                _latlng = onValue.latitude.toString()+","+onValue.longitude.toString();
                _radius = FuncoesExtras.raioBusca(data.raioBusca.toString()).replaceAll("KM", "");
                _radius = _radius.replaceAll("Distância: ", "");
                _radius = _radius == "SEM LIMITE" ? "0" : _radius;

              } else {
                _latlng = "0";
                _radius = "0";
              }

              print("LATLNG: "+_latlng);

              _diasRetro = FuncoesExtras.raioTempo(data.filtroTempo.toString()).replaceAll(" DIAS", "");
              _diasRetro = _diasRetro.replaceAll("Atualizado em: ", "");
              _diasRetro = _diasRetro.replaceAll(" DIA", "");
              _city = data.municipio;
              
              Map<String, dynamic> mapSearch = {};
              mapSearch['params'] = _params;
              mapSearch['city'] = _city;
              mapSearch['radius'] = _radius;
              mapSearch['latlng'] = _latlng;
              mapSearch['diasRetro'] = _diasRetro;

              if(blocSearch.isDisposed){
                blocSearch.dispose();
                blocSearch.initListeners();
              }
              blocSearch.inSearch.add(mapSearch);

              blocSearch.outLista.listen((data){
                setState(() {
                  items = data;
                  isPerformingRequest = false;
                });
              });
            });
        }
      } catch(err){
        print(err.toString());
      }

    });

     _scrollController.addListener(() async {
      if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        setState(() => isPerformingRequest = true);
        await blocSearch.nextPage();
        setState(() => isPerformingRequest = false);
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    blocLista.dispose();
    blocPrefs.dispose();
    blocSearch.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.lightBlue[300],
      appBar: FuncoesExtras.getTitleAppBar(context, false),
      body: Stack(
            children: <Widget>[
              items.length == 0 && isPerformingRequest ? Center(
                child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                  ),
              ) 
              : ListView.builder(
                  controller: _scrollController,
                  itemCount: items.length + 1,
                  padding: EdgeInsets.only(top: 115),
                  itemBuilder: (context, index){
                  if (index < items.length) {
                    return ProdutoTile(snapshot: items[index], isCombustivel: _isCombustivel,func: (){
                      FuncoesExtras.addToList(items[index], context);
                    },);
                  } else if (index > 1 && isPerformingRequest) {
                    return Container(
                        height: 40,
                        width: 40,
                        alignment: Alignment.center,
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                        ),
                      );
                  } else {
                    return Container();
                  }
                  },
                ),
              Container(
                height: 80,
                color: Theme.of(context).accentColor,
                padding: EdgeInsets.only(bottom: 0, left: 10, right: 10),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      height: 60,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(const Radius.circular(15.0),)
                          ),
                          child: Center(
                            child: ListTile(
                                  leading: Icon(FontAwesomeIcons.searchDollar, size: 20, color: Theme.of(context).primaryColor,),
                                  title: Text(_params, style: TextStyle(fontSize: 25, color: Theme.of(context).primaryColor), textAlign: TextAlign.center,),
                                  trailing: IconButton(
                                    icon: Icon(Icons.close),
                                    iconSize: 20,
                                    color: Theme.of(context).primaryColor,
                                    onPressed: (){
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ),
                          ),
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                        margin: EdgeInsets.symmetric(vertical: 80),
                        height: 35,
                        width: MediaQuery.of(context).size.width * 0.85,
                        decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            borderRadius: BorderRadius.only(bottomLeft:  const Radius.circular(20.0), bottomRight:  const Radius.circular(20.0))
                            ),
                            child: Center(
                              child: Text( 
                                isPerformingRequest ? "aguarde..." :
                                (blocSearch.totalResultados.toString() + (blocSearch.totalResultados > 1 ? " resultados exibidos" : " resultado exibido"))
                                , style: TextStyle(color: Colors.white, fontSize: 14),
                                ),
                            ),
                      ),
                ],
              ),
            ],
          ),
      bottomNavigationBar: BarraBotoesReduzida(func: (){
                        setState(() {
                          Navigator.of(context).pop();
                        });
                      }, stateSheet: false,)
    );
  }

  // void _addToList(Produto produto){
  //   Navigator.of(context).push(
  //     MaterialPageRoute(builder: (context) => ListaComprasScreen(produto: produto,))
  //   );
  // }

}