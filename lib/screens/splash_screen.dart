import 'dart:async';

import 'package:economizarr/screens/home_screen.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();

  loadData();
  }

  Future<Timer> loadData() async {
  return new Timer(Duration(seconds: 5), onDoneLoading);
}

onDoneLoading() async {
  Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => HomeScreen()));
}

  @override
  Widget build(BuildContext context) {
    return  Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
          // Where the linear gradient begins and ends
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          // Add one stop for each color. Stops should increase from 0 to 1
          stops: [0.1, 0.5, 0.7, 0.9],
          colors: [
            // Colors are easy thanks to Flutter's Colors class.
            Colors.lightBlue[300],
            Colors.lightBlue[200],
            Colors.lightBlue[100],
            Colors.lightBlue[50],
          ],
        ),
          // color: Colors.white
            // image: DecorationImage(
            //     image: AssetImage("images/background.jpg"), fit: BoxFit.cover
            //     )
      ),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height *0.3),
                  child: Center(
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.3,
                      height: MediaQuery.of(context).size.height * 0.3,
                      child: FlareActor("assets/animSplash.flr", animation: "pulse",)),
                            // child: CircularProgressIndicator(
                            //   valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
                            // ),
                          ),
                ),
              ],
            ),
            Container(
                  color: Colors.lightBlue[300],
                  margin: EdgeInsets.only(bottom: 0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Container(padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),margin: EdgeInsets.all(5),
                        child: Row(mainAxisSize: MainAxisSize.max, mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Image.asset("images/governo.webp", width: 150,),
                            Image.asset("images/sefaz.webp", width: 120,),
                        ],),
                      )
                    ],
                  ),
                ),
          ],
        )
      ),
    );
  }
}