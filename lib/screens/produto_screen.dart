import 'package:economizarr/model/produto_model.dart';
import 'package:economizarr/utils/funcoes_extras.dart';
import 'package:economizarr/widgets/barra_botoes_reduzida.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ProdutoScreen extends StatefulWidget {

  final Produto produto;

  ProdutoScreen({Key key, @required this.produto}) : super(key: key);

  @override
  _ProdutoScreenState createState() => _ProdutoScreenState(produto);
}

class _ProdutoScreenState extends State<ProdutoScreen> {

  Produto _produto;

  _ProdutoScreenState(Produto produto){
    this._produto = produto;
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.lightBlue[300],
      appBar: FuncoesExtras.getTitleAppBar(context, false),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(const Radius.circular(15.0)),
                            ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Center(child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Text(_produto.produto, style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                  )),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.7,
                    margin: EdgeInsets.only(bottom: 15),
                    decoration: BoxDecoration(
                            color: Colors.lightGreen,
                            borderRadius: BorderRadius.all(const Radius.circular(15.0)),
                            ),
                            child: Container(
                              height: 70,
                              margin: EdgeInsets.all(20),
                              decoration: BoxDecoration(
                                color: Colors.lightGreen[300],
                                borderRadius: BorderRadius.all(const Radius.circular(25.0)),
                                ),
                                child: Center(child: Text("R\$ "+ double.parse(_produto.preco).toStringAsFixed(2), 
                                  style: TextStyle(color: Colors.white, fontSize: 35, fontWeight: FontWeight.bold),)),
                  ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.7,
                    margin: EdgeInsets.only(bottom: 15),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                      Text("ATUALIZADO HÁ " + FuncoesExtras.getTempoAtualizado(_produto.dataPreco), style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13)),
                      Text("VER HISTÓRICO", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13))
                    ],),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.7,
                    margin: EdgeInsets.only(bottom: 5),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                      Icon(Icons.store, size: 50, color: Theme.of(context).accentColor,),
                      Flexible(child: Text(_produto.empresa, style: TextStyle(fontSize: 18, color: Colors.grey)))
                    ],),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.7,
                    margin: EdgeInsets.only(bottom: 10),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                      Icon(Icons.pin_drop, size: 50, color: Theme.of(context).accentColor,),
                      Flexible(child: Text(_produto.logra + ", " + _produto.endNum + "\n" + _produto.bairro + "\n" + (_produto.distanciaKm == null ? " " : _produto.distanciaKm.toStringAsFixed(2) +"KM"), style: TextStyle(fontSize: 16, color: Colors.grey)))
                    ],),
                  ),
                  Flexible(
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.8,
                      margin: EdgeInsets.only(bottom: 10),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                        Flexible(
                          child: Container(
                            decoration: BoxDecoration(
                                color: Theme.of(context).accentColor,
                                borderRadius: BorderRadius.all(const Radius.circular(25.0)),
                                ),
                            child: FlatButton.icon(icon: Icon(FontAwesomeIcons.directions), onPressed: (){
                              FuncoesExtras.abrirMaps(_produto);
                            }, label: Text("Como Chegar"),textColor: Colors.white,)),
                        ),
                          Flexible(
                            child: Container(
                            decoration: BoxDecoration(
                                color: Theme.of(context).accentColor,
                                borderRadius: BorderRadius.all(const Radius.circular(25.0)),
                                ),
                            child: FlatButton.icon(icon: Icon(Icons.add_shopping_cart), onPressed: (){
                              FuncoesExtras.addToList(_produto, context);
                            }, label: Text("Adic. à Lista"),textColor: Colors.white,)),
                          )
                      ],),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BarraBotoesReduzida(func: (){
        setState(() {
          Navigator.of(context).pop();
        });
      }, stateSheet: false,),
    );
  }
}