import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:economizarr/blocs/listacompra_bloc.dart';
import 'package:economizarr/model/listacompra_model.dart';
import 'package:economizarr/model/produto_model.dart';
import 'package:economizarr/screens/listaprodutos_screen.dart';
import 'package:economizarr/utils/funcoes_extras.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ListaComprasScreen extends StatefulWidget {

  final Produto produto;

  ListaComprasScreen({this.produto});

  @override
  _ListaComprasScreenState createState() => _ListaComprasScreenState(this.produto);
}

class _ListaComprasScreenState extends State<ListaComprasScreen> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final blocLista = BlocProvider.getBloc<ListaCompraBloc>();

  Timer _timerToSnackbar;

  Produto _produto;

  _ListaComprasScreenState(produto){
    _produto = produto;
  }

  @override
  void dispose() {
    _produto = null;
    if(_timerToSnackbar != null)
      _timerToSnackbar.cancel();
    blocLista.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    blocLista.getListaCompras();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.lightBlue[300],
      appBar: FuncoesExtras.getTitleAppBar(context, false),
      body: Stack(
        children: <Widget>[
          _produto != null ? Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                    margin: EdgeInsets.only(top: 63),
                    height: 35,
                    width: MediaQuery.of(context).size.width * 0.85,
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.only(bottomLeft:  const Radius.circular(20.0), bottomRight:  const Radius.circular(20.0))
                        ),
                        child: Center(
                          child: Text( 
                            _produto.produto, style: TextStyle(color: Colors.white, fontSize: 14),
                            ),
                        ),
                  ),
            ],
          ) : Container(),
          Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              GestureDetector(
                child: Center(
                  child: Container(
                    margin: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                    padding: EdgeInsets.all(10),
                    height: 50,
                    decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(20)), color: Colors.white,),
                    child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                              Icon(Icons.add, color: Theme.of(context).primaryColor, size: 20,),
                              Text("Nova Lista", style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 20),),
                          ],
                        ),
                  ),
                ),
                onTap: () async {
                  _showModalNovaLista();
                },
              ),
              StreamBuilder<List<ListaCompra>>(
                stream: blocLista.outLista,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Flexible(
                      child: Container(
                        margin: EdgeInsets.only(top: _produto != null ? 20 : 0),
                        child: ListView.builder(
                          itemCount: snapshot.data.length,
                          itemBuilder: (context, index) {
                            ListaCompra lista = snapshot.data[index];
                            return Container(
                              margin: EdgeInsets.symmetric(horizontal: 20),
                              child: Card(
                                  clipBehavior: Clip.antiAlias,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8.0)),
                                      color: Colors.white,
                                      child: GestureDetector( 
                                        behavior: HitTestBehavior.translucent,
                                        onTap: _produto != null ? () async {
                                          await blocLista.addToList(_produto, lista);
                                          final snackBar = SnackBar(
                                            content: Text('Adicionado na lista'),
                                            action: SnackBarAction(
                                              label: "Voltar",
                                              onPressed: (){Navigator.of(context).pop();},
                                            ),
                                          );

                                          _scaffoldKey.currentState.showSnackBar(snackBar);
                                          _timerToSnackbar = new Timer(const Duration(milliseconds: 1000), () {
                                              setState(() {
                                                Navigator.of(context).pop();
                                              });
                                            });
                                          // Navigator.of(context).pop();
                                        } :
                                        (){
                                          Navigator.of(context).push(
                                            MaterialPageRoute(builder: (context)=>ListaProdutosScreen(listaCompra: lista,))
                                            ).then((val){
                                              blocLista.getListaCompras();
                                            });
                                        },
                                        child: Row(
                                            mainAxisSize: MainAxisSize.max,
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Column(
                                                mainAxisSize: MainAxisSize.max,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Container(margin: EdgeInsets.only(left: 20, top: 10, bottom: 5),child: Text(lista.nome, style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 25, fontWeight: FontWeight.bold),)),
                                                  Container(margin: EdgeInsets.only(left: 20, top: 5, bottom: 2),child: Text( (lista.itensCompra != null ? lista.itensCompra.length.toString() : "0") + " itens (máximo 40)", style: TextStyle(color: Theme.of(context).accentColor))),
                                                  Container(margin: EdgeInsets.only(left: 20, top: 3, bottom: 10),child: lista.itensCompra != null && lista.itensCompra.length > 0 ? 
                                                    Text("Total: R\$ "+ (lista.itensCompra != null ? _somarItensLista(lista) : "0.00") + " (valor estimado)", style: TextStyle(color: Theme.of(context).accentColor)):Text("Inclua produtos na sua lista",style: TextStyle(color: Theme.of(context).accentColor),)),
                                                ],
                                              ),
                                              Container(
                                                color: Colors.blue,
                                                width: 50,
                                                height: 100,
                                                child:  IconButton(color: Colors.white, icon: Icon(FontAwesomeIcons.trash), onPressed: () async {
                                                  await blocLista.delete(snapshot.data[index].id);
                                                  setState(() {
                                                        List.from(snapshot.data)..removeAt(index);
                                                      });
                                                }),
                                              ),
                                            ],
                                          ),
                                      ),
                                ),
                            );
                          },
                        ),
                      ),
                    );
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                },
              ),
            ],
          ),
        ],
      ),
    );
  }

  _somarItensLista(ListaCompra lista){
    double total = 0.0;
    for(Produto item in lista.itensCompra){
      total += (double.parse(item.preco) * item.qtdeItens);
    }
    String totalStr = total.toStringAsFixed(2);
    return totalStr;
  }

  Future _showModalNovaLista() async {
      await showDialog(
        context: context,
        builder: (context){
          final _controller = TextEditingController();
          bool _disabled = true;

          return StatefulBuilder(
            builder: (context, setState) {
            return SimpleDialog(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
              title: new Text("Lista de Compras", 
                style: TextStyle(
                  color: Theme.of(context).dividerColor, 
                  fontSize: 22, 
                  fontWeight: FontWeight.bold), 
                  textAlign: TextAlign.center,),
              children: <Widget>[
                Divider(
                    thickness: 1.0,
                    indent: 25,
                    endIndent: 25,
                    color: Theme.of(context).dividerColor,
                  ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    new SimpleDialogOption(
                      child: TextField(
                        controller: _controller,
                        textAlign: TextAlign.center,
                        textAlignVertical: TextAlignVertical.center,
                        style: TextStyle(fontSize: 20, color: Theme.of(context).dividerColor),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'NOME DA LISTA',
                          hintMaxLines: 1,
                          hintStyle: TextStyle(color: Colors.grey[300]),
                          alignLabelWithHint: true
                        ),
                        onChanged: (val){
                          setState(() {
                            if(_controller.text.isEmpty)
                              _disabled = true;
                            else
                              _disabled = false;
                          });
                        },
                      ),
                    ),

                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        FlatButton.icon(
                            icon: Icon(Icons.save, color: Colors.white,),
                            label: Text("Salvar", style: TextStyle(color: Colors.white),),
                            color: Colors.blue,
                            disabledColor: Colors.blueGrey,
                            disabledTextColor: Colors.white,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0),), 
                            onPressed: _disabled ? null : () async {
                              ListaCompra lista = ListaCompra(nome: _controller.text);
                              await blocLista.insert(lista);
                              Navigator.of(context).pop();
                            },),
                        FlatButton.icon(
                            icon: Icon(Icons.cancel, color: Colors.white,),
                            label: Text("Cancelar", style: TextStyle(color: Colors.white),), 
                            color: Colors.blue,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0),), onPressed: (){
                              Navigator.of(context).pop();
                            },)
                      ],
                    )
                  ],
                )
              ],
            );
          }
        );
      },
      
    );
  }

}