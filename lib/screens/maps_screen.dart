// import 'package:economizarr/model/produto_model.dart';
// import 'package:economizarr/utils/funcoes_extras.dart';
// import 'package:economizarr/utils/staticmap.dart';
// import 'package:flutter/material.dart';
// import 'package:map_view/map_view.dart';

// class MapsScreen extends StatefulWidget {
  
//   final Produto produto;

//   MapsScreen({@required this.produto});

//   @override
//   _MapsScreenState createState() => _MapsScreenState(produto);
// }

// class _MapsScreenState extends State<MapsScreen> {
//   static Produto _produto;

//   _MapsScreenState(Produto produto){
//     _produto = produto;
//   }
// MapView mapView = new MapView();
//   var staticMapProvider = new StaticMapProvider(FuncoesExtras.API_KEY);
//   String staticMapUrl;

//   //experimental
//   var staticMap = StaticMap(FuncoesExtras.API_KEY);

//   @override
//   initState() {
//     super.initState();

//     // calling the custom class
//     staticMapUrl = staticMap.urlWithMarkers("1", Locations.portland, 12, 600, 400, Colors.red);
//     //print(staticMapUrl.toString());
    
//   }

//   @override
//   Widget build(BuildContext context) {
//     return new MaterialApp(
//       home: new Scaffold(
//           appBar: new AppBar(
//             title: new Text('Map View Example'),
//           ),
//           body: new Column(
//             mainAxisAlignment: MainAxisAlignment.start,
//             children: <Widget>[
//               new Container(
//                 height: 250.0,
//                 child: new Stack(
//                   children: <Widget>[
//                     new Center(
//                         child: new Container(
//                       child: new Text(
//                         "You are supposed to see a map here.\n\nAPI Key is not valid.\n\n"
//                             "To view maps in the example application set the "
//                             "API_KEY variable in example/lib/main.dart. "
//                             "\n\nIf you have set an API Key but you still see this text "
//                             "make sure you have enabled all of the correct APIs "
//                             "in the Google API Console. See README for more detail.",
//                         textAlign: TextAlign.center,
//                       ),
//                       padding: const EdgeInsets.all(20.0),
//                     )),
//                     new InkWell(
//                       child: new Center(
//                         child: new Image.network(staticMapUrl.toString()),
//                       ),
//                       onTap: showMap,
//                     )
//                   ],
//                 ),
//               ),
//               new Container(
//                 padding: new EdgeInsets.only(top: 10.0),
//                 child: new Text(
//                   "Tap the map to interact",
//                   style: new TextStyle(fontWeight: FontWeight.bold),
//                 ),
//               ),       
//             ],
//           )),
//     );
//   }

//   showMap() {
//     mapView.show(
//         new MapOptions(
//             mapViewType: MapViewType.normal,
//             showUserLocation: true,
//             showMyLocationButton: true,
//             showCompassButton: true,
//             initialCameraPosition: new CameraPosition(
//                 new Location(45.526607443935724, -122.66731660813093), 15.0),
//             hideToolbar: false,
//             title: "Recently Visited"),
//         toolbarActions: [new ToolbarAction("Close", 1)]);
    
//     mapView.onLocationUpdated.listen((location) {
//       print("Location updated $location");
//     });


//     mapView.onToolbarAction.listen((id) {
//       print("Toolbar button id = $id");
//       if (id == 1) {
//         _handleDismiss();
//       }
//     });
  
//   }

//   _handleDismiss() async { 
//     var uri = await staticMap.getImageUrlFromMap(mapView);
//     setState(() => staticMapUrl = uri.toString());    
//     mapView.dismiss();
//   }

// }