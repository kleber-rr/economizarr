import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:economizarr/blocs/listaproduto_bloc.dart';
import 'package:economizarr/model/listacompra_model.dart';
import 'package:economizarr/model/produto_model.dart';
import 'package:economizarr/utils/funcoes_extras.dart';
import 'package:economizarr/widgets/contador.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ListaProdutosScreen extends StatefulWidget {

  final ListaCompra listaCompra;

  ListaProdutosScreen({this.listaCompra});

  @override
  _ListaProdutosScreenState createState() => _ListaProdutosScreenState(this.listaCompra);
}

class _ListaProdutosScreenState extends State<ListaProdutosScreen> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final blocLista = BlocProvider.getBloc<ListaProdutoBloc>();


  ListaCompra _listaCompra;

  _ListaProdutosScreenState(listaCompra){
    _listaCompra = listaCompra;
  }

  @override
  void dispose() {
    blocLista.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    blocLista.getProdutos(_listaCompra.id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.lightBlue[300],
      appBar: FuncoesExtras.getTitleAppBar(context, false),
      body: Stack(
        children: <Widget>[
          _listaCompra != null ? Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                    margin: EdgeInsets.only(top: 63),
                    height: 35,
                    width: MediaQuery.of(context).size.width * 0.85,
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.only(bottomLeft:  const Radius.circular(20.0), bottomRight:  const Radius.circular(20.0))
                        ),
                        child: Center(
                          child: Text( 
                            _listaCompra.nome, style: TextStyle(color: Colors.white, fontSize: 20),
                            ),
                        ),
                  ),
            ],
          ) : Container(),
          Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              GestureDetector(
                child: Center(
                  child: Container(
                    margin: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                    padding: EdgeInsets.all(10),
                    height: 50,
                    decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(20)), color: Colors.white,),
                    child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                              Icon(Icons.add, color: Theme.of(context).primaryColor, size: 20,),
                              Text("Nova Lista", style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 20),),
                          ],
                        ),
                  ),
                ),
                onTap: () async {
                  // _showModalNovaLista();
                },
              ),
              StreamBuilder<List<Produto>>(
                stream: blocLista.outLista,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Flexible(
                      child: Container(
                        margin: EdgeInsets.only(top: _listaCompra != null ? 20 : 0),
                        child: ListView.builder(
                          itemCount: snapshot.data.length,
                          itemBuilder: (context, index) {
                            Produto item = snapshot.data[index];
                            return Container(
                              margin: EdgeInsets.symmetric(horizontal: 20),
                              child: Card(
                                  clipBehavior: Clip.antiAlias,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8.0)),
                                      color: Colors.white,
                                      child: GestureDetector( 
                                        behavior: HitTestBehavior.translucent,
                                        onTap: () {},
                                        child: Row(
                                            mainAxisSize: MainAxisSize.max,
                                            mainAxisAlignment: MainAxisAlignment.end,
                                            children: <Widget>[
                                              Container(
                                                child: Flexible(
                                                  fit: FlexFit.tight,
                                                  child: Column(
                                                    mainAxisSize: MainAxisSize.max,
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: <Widget>[
                                                      Container(margin: EdgeInsets.only(left: 20, top: 10, bottom: 5),child: Text(item.produto, style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 20, fontWeight: FontWeight.bold),)),
                                                      Container(margin: EdgeInsets.only(left: 20, top: 5, bottom: 2),child: Text( item.empresa, style: TextStyle(color: Theme.of(context).accentColor))),
                                                      Container(margin: EdgeInsets.only(left: 20, top: 3, bottom: 10),child:
                                                        Text("Total: R\$ "+ (
                                                          (item.qtdeItens != null && item.qtdeItens > 0 ? item.qtdeItens : 1)
                                                         * double.parse(item.preco)).toStringAsFixed(2) + " (valor estimado)", style: TextStyle(color: Theme.of(context).accentColor))),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              ButtonPlusMinusCounter(produto: item, funcUpdate: _atualizarProduto),
                                              Container(
                                                margin: EdgeInsets.only(left: 10),
                                                color: Colors.blue,
                                                width: 50,
                                                height: 115,
                                                child:  IconButton(color: Colors.white, icon: Icon(FontAwesomeIcons.trash), onPressed: () async {
                                                  await blocLista.delete(item);
                                                  setState(() {
                                                        List.from(snapshot.data)..removeAt(index);
                                                      });
                                                }),
                                              ),
                                            ],
                                          ),
                                      ),
                                ),
                            );
                          },
                        ),
                      ),
                    );
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                },
              ),
            ],
          ),
        ],
      ),
    );
  }

  _atualizarProduto(Produto item) async {
      await blocLista.update(item);
  }

}